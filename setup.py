from setuptools import setup, find_packages
from foo import __version__
setup(
    name="foo",
    version=__version__,
    packages=find_packages(exclude=["tests"]),
    author="Me",
    install_requires=['pandas'],
    extra_require={
        "dev": ["python-language-server[all]"],
        "test": ["pytest", "pyflakes"]
    },

)
