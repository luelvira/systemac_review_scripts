import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

def save_image(df, column, title, xlabel, ylabel, bins, figsize=(15, 5)):
    fig, ax = plt.subplots(figsize=figsize)
    #convert the column to string and sort it
    df[column] = df[column].astype(str)
    df[column] = df[column].sort_values()
    counts, bins, patches = ax.hist(df[column], bins, width=1, align='left', edgecolor='black')
    # ax.xlabel(column)
    # ax.ylabel(ylabel)
    ax.set_xticks(bins)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    if not os.path.exists('fig'):
        os.makedirs('fig')
    file_name = column.replace(' ', '_')
    fig.savefig('fig/' + file_name + '.png')

cur_dir = os.walk(".")
data_files = []
for root, dirs, files in cur_dir:
    if root == '.':
        for file in files:
            if file.endswith('.csv'):
                data_files.append(pd.read_csv(file))

df = pd.concat(data_files, ignore_index=True)
print(df.shape)

df = df.drop_duplicates(subset=['DOI'], keep='first')
# print(df.keys())

# Get the number of items in each year
print(df['Publication Year'].value_counts())
# Plot the number of items in each year

if os.environ.get('SAVE') == 'true':
    save_image(df, 'Publication Year', 'Number of Items per Publication Year', 'Publication Year', 'Number of Items', len(df['Publication Year'].unique()))
    save_image(df, 'Item Type', 'Number of Items per Item Type', 'Item Type', 'Number of Items', len(df['Item Type'].unique()), figsize=(5, 5))


